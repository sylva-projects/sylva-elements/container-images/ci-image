ARG SYLVA_TOOLBOX_TAG
FROM registry.gitlab.com/sylva-projects/sylva-elements/container-images/sylva-toolbox:$SYLVA_TOOLBOX_TAG
LABEL \
	maintainer="Sylva team" \
	repo="https://gitlab.com/sylva-projects/sylva-elements/container-images/ci-image"

# renovate: datasource=github-releases depName=getsops/sops
ARG SOPS_VERSION=v3.9.4
# renovate: datasource=github-releases depName=sigstore/cosign
ARG COSIGN_VERSION=v2.4.3

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

# hadolint ignore=DL3018, DL3013
RUN apk update \
  && apk upgrade \
  && apk add --no-cache curl bash coreutils git gettext yamllint py3-yaml py3-jsonschema py3-pip docker openrc crane jq diffutils uuidgen \
  && apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing hurl \
  && rc-update add docker boot \
  && wget -q --show-progress --progress=bar:force -c https://github.com/getsops/sops/releases/download/${SOPS_VERSION}/sops-${SOPS_VERSION}.linux.amd64 -O /usr/local/bin/sops \
  && chmod +x /usr/local/bin/sops \
  && wget -q --show-progress --progress=bar:force -c https://github.com/sigstore/cosign/releases/download/${COSIGN_VERSION}/cosign-linux-amd64 -O /usr/local/bin/cosign \
  && chmod +x /usr/local/bin/cosign
