# CI images

This repo contains the Dockerfiles and the needed script to build CI images used in Sylva.

## ci-image

Base image used in CI mostly for Sylva-core deployment

## ci-release-image

Image used by the release-note template.
Contain a python script to generate the release note.