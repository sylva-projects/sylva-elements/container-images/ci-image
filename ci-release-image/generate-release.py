#!/usr/bin/env python

import requests
import subprocess
import argparse
import logging
import re
import sys

# Setup logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

parser = argparse.ArgumentParser(description='Generate report based on MR merged between 2 git tags')
parser.add_argument('--project-id',
                    dest='project_id',
                    required=True,
                    help='Gitlab project id.')
parser.add_argument('--starting-tag',
                    dest='starting_tag',
                    required=True,
                    help='First tag to look at')
parser.add_argument('--end-tag',
                    dest='end_tag',
                    required=True,
                    help='Last tag to look at')
parser.add_argument('--token',
                    dest='private_token',
                    required=True,
                    help='Read only token (to avoid rate limitation)')
parser.add_argument('--output',
                    dest='output_file',
                    required=False,
                    default="release-notes.md",
                    help='Redirect output in a file, default to release-notes.md')
parser.add_argument('--write-mode',
                    dest='write_mode',
                    required=False,
                    default="a",
                    help='Write mode for output file ("a" for append, "w" for overwrite)')
args = parser.parse_args()

project_id = args.project_id
starting_tag = args.starting_tag
end_tag = args.end_tag
private_token = args.private_token
output_file = args.output_file

excluded_contributors_match = r'([rR]enovate [bB]ot|[Gh]host [Uu]ser)'

# Define sections and associated labels
# The sections order is important as the code uses a first match
# e.g we have many MRs with CI & CAPO labels, ideally we want to put them under CI section
# as they were probably just CI fix for CAPO jobs
sections = {
    "Breaking changes": ["API-breaking-change"],
    "CI": ["CI", "CI-bugs"],
    "CAPO": ["capo"],
    "CAPD": ["capd"],
    "CAPM3": ["capm3", "baremetal"],
    "CAPV": ["capv"],
    "RKE2": ["rke2"],
    "Kubeadm": ["kubeadm"],
    "OKD/OpenShift": ["okd"],
    "Features": ["feature", "enhancement", "type::feature", "type::enhancement"],
    "Security": ["security"],
    "Monitoring & logging": ["monitoring", "logging"],
    "Storage": ["longhorn"],
    "Lifecyle": ["cluster-lifecycle"],
    "Bug Fixes": ["bug", "fix", "type::bug"],
    "Cleanups": ["type::cleanup", "type::tech-debt"],
    "Documentation": ["documentation", "docs"],
    "Other dependency upgrades": ["renovate"],
    "Other": [],  # This will hold merge requests that do not match any other section
}

# Define the order in which sections should be displayed
# Display order may be different from filter order
section_order = [
    "Features",
    "Breaking changes",
    "Bug Fixes",
    "Monitoring & logging",
    "Storage",
    "Security",
    "Lifecyle",
    "CAPO",
    "CAPD",
    "CAPM3",
    "CAPV",
    "RKE2",
    "Kubeadm",
    "OKD/OpenShift",
    "Documentation",
    "CI",
    "Cleanups",
    "Other",
    "Other dependency upgrades",
]

# fail if a section isn't listed in section_order
missing_from_section_order = set(section_order) - set(sections.keys())
if missing_from_section_order:
    logging.error(f"the following sections are missing from 'section_order': {missing_from_section_order}")
    sys.exit(1)

# List of technical labels that we don't want to display in the release notes
global_hidden_labels = [
    'run-e2e-tests',
    'delay-capo-ci-cleanup-on-failure',
    'priority::.*',
    'weight::.*',
    'AutoMerge.*',
    'status::.*',
    'ci-feature::.*',
    'CI-breakage',
    'dependencies::.*',
    'ci-investigation-needed',
]

# If a MR has a label from this list, we'll not display the MR in the release notes
excluded_labels = []

# Get commit range in one call
tag_filter = f'{starting_tag}...{end_tag}'

# Get the date of the starting tag
start_tag_date_command = f'git log -1 --format=%aI {starting_tag}'
start_tag_date_result = subprocess.run(start_tag_date_command, shell=True, stdout=subprocess.PIPE, text=True)
start_tag_date_str = start_tag_date_result.stdout.strip()

logging.info(f'Starting tag date: {start_tag_date_str}')

# Get all commits between the two tags
logging.info('Fetching commits between ' + tag_filter)
commits = subprocess.run(f'git log {tag_filter} --pretty=format:%H --merges', shell=True, stdout=subprocess.PIPE,
                         text=True)
commit_hashes = set(commits.stdout.splitlines())

# GitLab API endpoint
endpoint = 'https://gitlab.com'
headers = {"PRIVATE-TOKEN": private_token}

# Fetch all merge requests
mr_url = f"{endpoint}/api/v4/projects/{project_id}/merge_requests"
params = {
    'state': 'merged',
    'per_page': 100,
    'updated_after': start_tag_date_str
}
merge_requests = []
page = 1

logging.info('Fetching merge requests...')
# Paginate through all merge requests
while True:
    params['page'] = page
    response = requests.get(mr_url, headers=headers, params=params)
    if response.status_code != 200:
        logging.error(f'Failed to fetch merge requests: {response.status_code} {response.text}')
        break
    data = response.json()
    if not data:
        break
    merge_requests.extend(data)
    logging.info(f'Processed page {page}')
    page += 1

# Filter merge requests by merge_commit_sha
logging.info('Filtering merge requests by merge_commit_sha...')
mrs_in_range = [mr for mr in merge_requests if mr['merge_commit_sha'] in commit_hashes]


# Function to extract component and version from MR title
def extract_component_version(title):
    match = re.match(r'update (.+?) to (.+)', title, re.IGNORECASE)
    if match:
        return match.group(1).strip(), match.group(2).strip()
    return None, None


# Group merge requests into sections
grouped_mrs = {section: [] for section in sections}
for mr in mrs_in_range:
    added = False
    for section, labels in sections.items():
        if any(lbl in mr['labels'] for lbl in labels):
            grouped_mrs[section].append(mr)
            added = True
            break
    if not added:
        grouped_mrs["Other"].append(mr)

# Sort merge requests by title within each section
for section in grouped_mrs:
    grouped_mrs[section].sort(key=lambda mr: mr['merged_at'])


# Group similar MRs by component update
def group_updates(mrs):
    updates = {}
    for mr in mrs:
        component, version = extract_component_version(mr['title'])
        if component:
            if component not in updates:
                updates[component] = []
            updates[component].append((mr, version))
    return updates


def escape_md_chars(text):
    return re.sub(r'([*_~])', r'\\\1', text)


def is_hidden_label(label, hidden_labels):
    return any([re.match(hidden_label, label) for hidden_label in hidden_labels])


def mr_desc(mr, omit_labels=[]):
    displayed_labels = [
        f'~"{lbl}"' for lbl in mr['labels'] if not is_hidden_label(lbl, global_hidden_labels + omit_labels)
    ]
    title = mr["title"]
    title = title.replace("s-c-c", "sylva-capi-cluster")
    return f"* {escape_md_chars(title)} [!{mr['iid']}]({mr['web_url']}) {' '.join(displayed_labels)}"


def write_md(*args, **kwargs):
    print(*args, file=f, **kwargs)


contributor_list = []


def add_contributor(mr_author):
    contributor_md = f"[{mr_author['name']}]({mr_author['web_url']})"
    if (not re.search(excluded_contributors_match, mr['author']['name']) and
            contributor_md not in contributor_list):
        contributor_list.append(contributor_md)


# Write the results to the output file
logging.info('Creating the release notes...')
with open(output_file, args.write_mode) as f:
    write_md("# Merge Requests integrated in this release")
    write_md("")
    # Display the grouped and sorted merge requests
    for section in section_order:
        mrs = grouped_mrs.get(section, [])
        if not mrs:
            logging.info(f"skipping section {section} because no MR matched")
            continue
        write_md(f"## {section}")
        write_md("")

        section_labels = sections[section]

        updates = group_updates(mrs)
        for component, mrs_versions in updates.items():
            if len(mrs_versions) > 1:
                final_version = mrs_versions[-1][1]
                write_md(f'* <details><summary>Update {component} to {final_version}</summary>')
                for mr, version in mrs_versions:
                    write_md("")
                    write_md("  " + mr_desc(mr, omit_labels=section_labels))
                write_md('</details>')
                write_md("")
            else:
                mr, version = mrs_versions[0]
                if any(lbl in mr['labels'] for lbl in excluded_labels):
                    continue
                write_md(mr_desc(mr, omit_labels=section_labels), flush=True)
                add_contributor(mr['author'])

        # For non-update MRs
        non_update_mrs = [mr for mr in mrs if not extract_component_version(mr['title'])[0]]
        for mr in non_update_mrs:
            if any(lbl in mr['labels'] for lbl in excluded_labels):
                continue
            write_md(mr_desc(mr, omit_labels=section_labels), flush=True)
            add_contributor(mr['author'])

        write_md("")

    write_md("## Contributors")
    write_md("")
    write_md(*contributor_list, sep=", ")

logging.info('A new release note was generated: ' + output_file)
